// This is in order to be able to require the theme which is in typescript
require("ts-node").register({
  compilerOptions: {
    // Because node does not understand import yet
    module: "commonjs"
  }
});
const { paramCase } = require("change-case");

const variables = require("./src/theme/variables");

function addNumberDash(target) {
  if (target.search(/\d+/) > 0) {
    return target.replace(/(\d+)/, "-$1");
  }
  return target;
}

module.exports.variables = Object.entries(variables).reduce(
  (themeVars, [name, value]) => {
    return {
      ...themeVars,
      [`@${paramCase(addNumberDash(name))}`]: value
    };
  },
  {}
);
