import React from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { routes } from "./utils/routes";
import { BookPage } from "./pages/Book/BookPage";
import { BookProvider } from "./contexts/BookContext";
import { NotFoundPage } from "./pages/NotFound";

export const App = () => {
  return (
    <BrowserRouter>
      <BookProvider>
        <Switch>
          <Route exact path={routes.book} component={BookPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </BookProvider>
    </BrowserRouter>
  );
};

export default App;
