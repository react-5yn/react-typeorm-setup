// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

// Custom definitions
export const colors = {
  primary: "#f9bc60",
  secondary: "#004643",
  background: "#001e1d",
  white: "#ffffff"
};

// Custom fonts
export const fonts = {
  header: "quicksand"
};

// Theme colors
export const primaryColor = colors.primary;
export const infoColor = colors.primary;
export const successColor = "#52c41a"; // success state color
export const warningColor = "#faad14"; // warning state color
export const errorColor = "#f5222d"; // error state color
export const fontSizeBase = "16px"; // major text font size
export const headingColor = "rgba(255, 255, 255, 0.85)"; // heading text color
export const textColor = "rgba(255, 255, 255, 0.85)"; // major text color
export const textColorSecondary = "rgba(255, 255, 255, .45)"; // secondary text color
export const disabledColor = "rgba(37, 111, 96, 1)"; // disable state color
export const borderRadiusBase = "4px"; // major border radius
export const borderColorSplit = colors.secondary; // major border color
export const borderColorBase = colors.secondary;
export const disabledBg = colors.secondary;

export const boxShadowBase = "0 2px 8px rgba(0, 0, 0, 0.15)"; // major shadow for layers
export const componentBackground = colors.secondary;

export const bodyBackground = colors.background;
export const layoutBodyBackground = colors.background;
export const layoutHeaderBackground = colors.primary;
export const layoutFooterBackground = layoutHeaderBackground;
export const layoutHeaderHeight = "64px";
export const layoutHeaderPadding = "0 38px";
export const layoutFooterPadding = "24px 50px";
export const layoutSiderBackground = layoutHeaderBackground;
export const layoutSiderBackgroundLight = colors.white;

export const inputBg = colors.secondary;

export const iconColor = colors.secondary;
