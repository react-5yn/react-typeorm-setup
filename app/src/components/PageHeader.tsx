import styled from "styled-components";
import { fonts, colors } from "../theme/variables";

export const PageHeader = styled.h1`
  font-family: ${fonts.header};
  color: ${colors.primary};
  text-align: center;
  font-size: 3rem;
  margin-bottom: -1rem;
`;
