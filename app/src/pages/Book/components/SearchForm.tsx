import React, { useContext } from "react";
import { Formik } from "formik";
import { Form, SubmitButton, Input } from "formik-antd";
import * as Yup from "yup";
import { Icon, Row, Col } from "antd";
import { bookContext } from "../../../contexts/BookContext";

const SearchSchema = Yup.object().shape({
  search: Yup.string()
    .max(30, "Please keep it a little shorter!")
    .required()
});

export const SearchForm: React.FC = () => {
  const { searchBooks } = useContext(bookContext);

  return (
    <Formik
      initialValues={{ search: "" }}
      validationSchema={SearchSchema}
      onSubmit={async ({ search }) => {
        try {
          await searchBooks(search);
        } catch (error) {
          console.log(error);
        }
      }}
    >
      {formik => (
        <Form onSubmit={formik.handleSubmit} style={{ margin: "2rem auto" }}>
          <Row type="flex" justify="center" style={{ flexWrap: "nowrap" }}>
            <Col>
              <Form.Item name="search" htmlFor="search">
                <Input
                  id="search"
                  name="search"
                  size="large"
                  placeholder="Search"
                />
              </Form.Item>
            </Col>
            <Col>
              <SubmitButton size="large">
                <Icon type="search" />
              </SubmitButton>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
