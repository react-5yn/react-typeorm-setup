import React from "react";
import { Card } from "antd";
import styled from "styled-components";
import { Book } from "../../../contexts/BookContext";

const BookAttributeName = styled.span`
  font-weight: bold;
`;

const BookCardFlex = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const BookDetails = styled.div`
  flex: 1 0 10rem;
`;

const BookAttribute = styled.p`
  margin: 0;
`;

const BookCardThumbnail = styled.div`
  margin: 0 1rem 1rem 0;
`;

type BookCardProps = {
  book: Book;
};

export const BookCard: React.FC<BookCardProps> = ({ book }) => {
  return (
    <Card style={{ height: "100%" }}>
      <BookCardFlex>
        <BookCardThumbnail>
          {
            <img
              src={book.thumbnail}
              alt="thumbnail"
              style={{ width: "128px" }}
            />
          }
        </BookCardThumbnail>
        <BookDetails>
          <BookAttribute>
            <BookAttributeName>Title:</BookAttributeName>{" "}
            <a href={book.previewLink}>{book.title}*</a>
          </BookAttribute>
          <BookAttribute>
            <BookAttributeName>Author:</BookAttributeName>{" "}
            {book.authors && book.authors[0]
              ? book.authors.map((author, i) => {
                  if (i && i + 1) {
                    return ", " + author;
                  } else {
                    return author;
                  }
                })
              : "-"}
          </BookAttribute>
          <BookAttribute>
            <BookAttributeName>Publisher:</BookAttributeName>{" "}
            {book.publisher ? book.publisher : "-"}
          </BookAttribute>
          <BookAttribute>
            <BookAttributeName>Published:</BookAttributeName>{" "}
            {book.publishedDate ? book.publishedDate.substr(0, 4) : "-"}
          </BookAttribute>
          <BookAttribute>
            <BookAttributeName>ISBN:</BookAttributeName>{" "}
            {book.isbn === "" || (book.isbn && book.isbn.length === 1)
              ? "-"
              : book.isbn}
          </BookAttribute>
        </BookDetails>
      </BookCardFlex>
    </Card>
  );
};
