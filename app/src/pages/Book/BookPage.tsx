import React, { useContext } from "react";
import { SearchForm } from "./components/SearchForm";
import styled from "styled-components";
import { BookCard } from "./components/BookCard";
import { colors, fonts } from "../../theme/variables";
import { Row, Col, Spin } from "antd";
import { bookContext } from "../../contexts/BookContext";
import { PageHeader } from "../../components/PageHeader";

const LoadingSpinHolder = styled.div`
  margin-top: 5rem;
  text-align: center;
`;

const ErrorMessage = styled.div`
  text-align: center;
  margin-top: 1rem;
  color: ${colors.primary};
`;

const BookLinkInfo = styled.div`
  margin-bottom: 2rem;
  text-align: center;
`;

const BookPageInfo = styled.p`
  text-align: center;
  font-size: 0.9rem;
  font-family: ${fonts.header};
`;

const BookPageWrapper = styled.div`
  padding: 2rem;
  height: 100%;
`;

export const BookPage: React.FC = () => {
  const { books, loading, error } = useContext(bookContext);

  return (
    <BookPageWrapper>
      <PageHeader style={!books ? { marginTop: "30vh" } : {}}>
        Bookfinder
      </PageHeader>
      <BookPageInfo>
        powered by{" "}
        <a href="https://developers.google.com/books/docs/overview">
          Google Books API{" "}
        </a>
      </BookPageInfo>
      <SearchForm />
      {error !== "" ? <ErrorMessage>ERROR: {error}</ErrorMessage> : null}
      {loading ? (
        <LoadingSpinHolder>
          <Spin tip="Loading ..."></Spin>
        </LoadingSpinHolder>
      ) : null}
      {books === null || loading ? null : (
        <div>
          <Row type="flex" justify="center">
            {books.map(book => {
              return (
                <Col
                  key={book.title.substr(0, 2) + book.isbn}
                  xs={24}
                  lg={12}
                  xxl={8}
                  style={{ padding: "0.5rem" }}
                >
                  <BookCard book={book} />
                </Col>
              );
            })}
          </Row>
          <BookLinkInfo style={{ color: colors.primary }}>
            * external Link to the Book on books.google.de
          </BookLinkInfo>
        </div>
      )}
    </BookPageWrapper>
  );
};
