import React from "react";
import { PageHeader } from "../components/PageHeader";

export const NotFoundPage: React.FC = () => {
  return (
    <div style={{ textAlign: "center", marginTop: "2rem" }}>
      <PageHeader>ZOMFG!</PageHeader>
      <p>404 - Page not found</p>
    </div>
  );
};
