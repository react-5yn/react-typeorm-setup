import React, { useState } from "react";

export type Book = {
  isbn: string | null;
  title: string;
  authors: string[];
  publisher: string;
  publishedDate: string;
  thumbnail: string;
  previewLink: string;
};

export type BookContext = {
  books: Book[] | null;
  loading: boolean;
  error: string;
  searchBooks: (searchValue: string) => Promise<void>;
};

const initialBookContext = {
  books: null,
  loading: false,
  error: "",
  searchBooks: async () => {}
};

export const bookContext = React.createContext<BookContext>(initialBookContext);

type searchResponse = { status: "ok"; data: Book[] };

export const BookProvider: React.FC = ({ children }) => {
  const [books, setBooks] = useState<Book[] | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  const searchBooks = async (searchValue: string) => {
    setLoading(true);
    const res = await fetch("/book?searchParam=" + searchValue, {
      method: "GET"
    });
    if (res.status === 200) {
      const { data } = (await res.json()) as searchResponse;
      setBooks(data);
      setLoading(false);
      setError("");
    } else {
      setBooks(null);
      setLoading(false);
      setError("Google Book API not reachable");
      throw new Error("Google Book API not reachable");
    }
  };

  return (
    <bookContext.Provider value={{ books, loading, error, searchBooks }}>
      {children}
    </bookContext.Provider>
  );
};
