const CracoAntDesignPlugin = require("craco-antd");
const theme = require("./themeToLess");
module.exports = {
  jest: {
    configure(config) {
      config.transformIgnorePatterns = [
        "/node_modules/(?!antd|rc-pagination|rc-calendar|rc-tooltip)/.+\\.js$"
      ];
      return config;
    }
  },
  plugins: [
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeTheme: theme.variables,
        options: {
          insert: function insertAtTop(element) {
            var parent = document.querySelector("head");
            // eslint-disable-next-line no-underscore-dangle
            var lastInsertedElement = window._lastElementInsertedByStyleLoader;
            if (!lastInsertedElement) {
              parent.insertBefore(element, parent.firstChild);
            } else if (lastInsertedElement.nextSibling) {
              parent.insertBefore(element, lastInsertedElement.nextSibling);
            } else {
              parent.appendChild(element);
            }
            // eslint-disable-next-line no-underscore-dangle
            window._lastElementInsertedByStyleLoader = element;
          }
        }
      }
    }
  ]
};
