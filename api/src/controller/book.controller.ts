/** Package imports */
import { Request, Response } from "express";
import Axios, { AxiosRequestConfig, AxiosResponse } from "axios";

type Book = {
  isbn: string;
  title: string;
  authors: string[];
  publisher: string;
  publishedDate: string;
  thumbnail: string;
  previewLink: string;
};

export class BookController {
  public static async getBooks(req: Request, res: Response): Promise<void> {
    if (req.query.searchParam && req.query.searchParam !== null) {
      const searchParam: string = req.query.searchParam;
      try {
        const requestOptions: AxiosRequestConfig = {
          method: "get",
          url: "https://www.googleapis.com/books/v1/volumes?q=" + searchParam,
          responseType: "json"
        };
        const apiResponse: AxiosResponse = await Axios.request(requestOptions);
        //console.log(apiResponse.data.items[0]);

        let books: Book[] = apiResponse.data.items.map(apiResponseData => {
          const book: Book = {
            isbn: apiResponseData.volumeInfo.industryIdentifiers
              ? apiResponseData.volumeInfo.industryIdentifiers.map(
                  industryIdentifier => {
                    if (industryIdentifier.type === "ISBN_13") {
                      return industryIdentifier.identifier;
                    } else if (industryIdentifier.type === "OTHER") {
                      return "-";
                    }
                  }
                )
              : null,
            title: apiResponseData.volumeInfo.title,
            authors: apiResponseData.volumeInfo.authors,
            publisher: apiResponseData.volumeInfo.publisher,
            publishedDate: apiResponseData.volumeInfo.publishedDate,
            thumbnail: apiResponseData.volumeInfo.imageLinks
              ? apiResponseData.volumeInfo.imageLinks.thumbnail
              : "https://www.kavitabookdistributors.com/wp-content/uploads/2018/04/download.png",
            previewLink: apiResponseData.volumeInfo.previewLink
          };
          return book;
        });

        res.send({ status: "ok", data: books });
        return;
      } catch (e) {
        res.status(500).send({ status: "internal_server_error" });
        return;
      }
    }
    res.status(400).send({ status: "bad_request" });
  }
}
