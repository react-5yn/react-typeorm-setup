import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import * as bodyParser from "body-parser";
import { globalRouter } from "./router/global.router";

/** Variables */
const app: express.Application = express();

/** Global middleware */
app.use(bodyParser.json());

/** Routes */
app.use("/", globalRouter);

/** Start Server */
app.listen(process.env.API_PORT, () => {
  console.log("Server started! \nON PORT: " + process.env.API_PORT);
});
