/** Package imports */
import { Router } from "express";
import { BookController } from "../controller/book.controller";

/** Variables */
export const bookRouter: Router = Router({ mergeParams: true });

/** Routes */
bookRouter.get("/", BookController.getBooks);
