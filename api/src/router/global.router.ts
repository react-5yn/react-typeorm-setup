/** Package imports */
import { Router } from "express";
import { bookRouter } from "./book.router";

/** Variables */
export const globalRouter: Router = Router({ mergeParams: true });

/** Routes */
globalRouter.use("/book", bookRouter);
